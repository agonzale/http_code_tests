#!/bin/bash
#
# The idea is to run a HTTP server in port 8000. URLs like:
#
#    * http://127.0.0.1:8000/500/
#    * http://127.0.0.1:8000/200/
#
docker run --rm -v $PWD:/usr/src/app -w /usr/src/app -p 8000:8000 django bash -c "python manage.py runserver 0.0.0.0:8000"
