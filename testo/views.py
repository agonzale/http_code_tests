from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def h200(request):
	return HttpResponse('200 All fine')

def h500(request):
	return HttpResponse('500 Error on pourpose', status=500)
