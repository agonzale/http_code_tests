# Simple 200, 500, ... code site

The idea is to run a HTTP server in port 8000. URLs like:

    * http://127.0.0.1:8000/500/
    * http://127.0.0.1:8000/200/

